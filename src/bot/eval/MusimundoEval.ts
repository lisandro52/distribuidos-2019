import { IEval } from "../../interfaces/EvalInterface";

export class MusimundoEval implements IEval {

  cssSelector = "a.mus-product-box";
  
  evalFunction(elements:HTMLElement[]): any[] {
    let a = [];
    // Por cada elemento del DOM que matchee con el cssSelector, extrae información adicional en un object[]
    elements.forEach(el => {
      let prod = {} as any;
      prod.name = el.querySelector('p.mus-pro-name').textContent.trim();
      prod.price = el.querySelector('p.mus-pro-price > span.mus-pro-price-number > span').textContent.trim().replace('$','').replace('.', '').replace(',', '.');
      prod.price = parseFloat(prod.price);
      a.push(prod);
    });
    return a;
  };

  evalPaginationFunction(): string[] {
    var text = (<HTMLElement>document.querySelector("#content > div.productListPage.bg-gray > div.container > div > div.col.span_9 > div.searchResultsGridComponent > div.mus-results-title > p")).innerText;
    var array = text.replace(/\u00a0/g, "").replace("|", "").split(" ");
    // Math.ceil(totalItems/itemsPerPage)
    var pages = Math.ceil(parseInt(array[2])/parseInt(array[0]));
    var arrayPages = [];
    var query = "?q=%3Arelevance&page={0}";
    for (var i = 0; i < pages; i++) {
      //https://www.musimundo.com/telefonia/telefonos-celulares/c/82?q=%3Arelevance&page=1
      arrayPages.push(document.URL + query.replace("{0}", i.toString()));
    }

    return arrayPages;
  }

}