import puppeteer from "puppeteer";
import { IEval } from "../interfaces/EvalInterface";
import { ScraperInterface } from "../interfaces/IScraper";
import { EvalFactory } from "./EvalFactory";

export class Scraper implements ScraperInterface {

  constructor() { }

  static async scrap(url: string, evalObject: IEval): Promise<any> {

    const browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] });

    const page = await browser.newPage();
    await page.goto(url);

    const data = await page.$$eval(evalObject.cssSelector, evalObject.evalFunction);

    await browser.close();

    return data;
  }

  /**
   * ```typescript
   * // Use example:
   * s.getPagination("Musimundo", "https://www.musimundo.com/telefonia/telefonos-celulares/c/82")
   * .then(response => {
   *   console.log(response);
   * });
   * ```
  **/
  async getPagination(siteName: string, categoryUrl: string): Promise<string[]> {
    const browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] });

    const page = await browser.newPage();
    await page.goto(categoryUrl);

    const evaluator = EvalFactory.GetEvaluator(siteName);

    const data = await page.evaluate(evaluator.evalPaginationFunction);

    await browser.close();

    return data;
  }

}