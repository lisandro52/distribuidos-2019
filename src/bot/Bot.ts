import express from "express";
import axios from "axios";
import _ from "underscore";
import config from "../config.json";
import { Scraper } from "./Scraper";
import { BotInterface } from "../interfaces/IBot";
import { EvalFactory } from "./EvalFactory";
import { IOrder } from '../interfaces/IOrder';


export class Bot implements BotInterface {

  executeOrder(scrapOrder: IOrder) {
    let orders = scrapOrder.categoryUrls.length;
    scrapOrder.categoryUrls.forEach(async (url) => {
      let response = await Scraper.scrap(url, EvalFactory.GetEvaluator(scrapOrder.siteName));
      console.log("Scrapeo de página finalizado. Enviando datos a la DB...");
      await this.sendDataToApi({
        "site": scrapOrder.siteName,
        "category": scrapOrder.categoryName,
        "data": JSON.stringify(response)
      }, 1);
      orders--;
      if (orders === 0) {
        console.log('Terminé. Reportando a skynet');
        this.reportToSkynet();
      }
    });
  }

  sendDataToApi(data: any, retries: number): Promise<any> {
    if (retries > 2) { return; }
    return axios.request({
      method: "POST",
      baseURL: `${config.api_url}:${config.api_port}`,
      url: config.api_endpoint,
      data
    }).catch(error => {
      console.log(`Reintentando...`);
      retries++;
      setTimeout(() => this.sendDataToApi(data, retries), 5 * 1000);
    });
  }

  async reportToSkynet() {
    await axios.get(`${config.skynet_url}:${config.skynet_port}${config.skynet_botready_endpoint}`, {
      params: {
        url: `${config.bots_url}:${PORT}`
      }})
      .then((value) => console.log('Registrado en Skynet'))
      .catch((reason) => {
        console.log('ERROR - Skynet no responde, volviendo a conectar...');
        setTimeout(() => this.reportToSkynet(), 10 * 1000);
      });
  }
}

const app = express();
app.use(express.json());
const bot = new Bot();

const argv = process.argv.splice(2);
const PORT = argv[0];

app.post(config.bots_scrap_endpoint, (req, res) => {
  console.log(req.body);
  bot.executeOrder({
    siteName: req.body.siteName,
    categoryName: req.body.categoryName,
    categoryUrls: req.body.categoryUrls
  } as IOrder)
  res.status(200).send('Scraping');
});

app.listen(PORT, () => {
  console.log(`Bot listening on port ${PORT}`);
});

bot.reportToSkynet();
