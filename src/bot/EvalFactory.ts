import { MusimundoEval } from "./eval/MusimundoEval";
import { IEval } from "../interfaces/EvalInterface";

export class EvalFactory {

    public static GetEvaluator(siteName: string): IEval {
        switch (siteName) {
            case "Musimundo":
                return new MusimundoEval();
            default:
                return new MusimundoEval();
        }
    }

}