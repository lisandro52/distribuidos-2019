import express from 'express';
import axios from "axios";
import _ from "underscore";
import config from "../config.json";
import scrapOrders from "../scrap.json";
import { Scraper } from "../bot/Scraper";
import { IOrder } from "../interfaces/IOrder";

/**
* Controlador de la red de bots.
*/
export class Skynet {
  scrapOrders: IOrder[] = [];
  private _scraper: Scraper = new Scraper();

  constructor() { }

  /**
  * Carga las órdenes de scrapping del archivo 'scrap.json'.
  */
  initJobs() {
    console.log('Comenzando la carga de tareas');
    _.forEach(scrapOrders.scrapSites, (site) => {
      _.forEach(site.categories, async (cat) => {
        let sites = await this._scraper.getPagination(site.siteName, cat.categoryUrl);
        this.scrapOrders.push({
          siteName: site.siteName,
          categoryName: cat.categoryName,
          categoryUrls: sites
        })
      })
    });
  }

  async sendScrap(botUrl: string, order: IOrder, retries: number) {
    if (retries > 2) {
      this.scrapOrders.push(order);
      return;
    }

    await axios.post(botUrl + config.bots_scrap_endpoint, order)
      .then((value) => console.log(`Bot scrapeando`))
      .catch((reason) => {
        console.log(reason);
        console.log('Volviendo a enviar tarea...');
        retries++;
        setTimeout(() => this.sendScrap(botUrl, order, retries), 10 * 1000);
      });
  }
}

const skynet = new Skynet();
skynet.initJobs()

const app = express();
app.use(express.json());
app.listen(config.skynet_port, () => {
  console.log(`Skynet listening on port ${config.skynet_port}`);
});

app.get(config.skynet_botready_endpoint, (req, res) => {
  if(skynet.scrapOrders.length > 0) {
    res.status(200).send(`Welcome back ${req.query.url}`);
    skynet.sendScrap(req.query.url, skynet.scrapOrders.pop(), 1)
  }
  else {
    res.sendStatus(503);
  }
});
