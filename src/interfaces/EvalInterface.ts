export interface IEval {
  cssSelector: string;
  evalFunction(elements: HTMLElement[]): any[];
  evalPaginationFunction(): string[];
}
