export interface ScraperInterface {
    getPagination(siteName: string, categoryUrl: string):  Promise<string[]>;
}