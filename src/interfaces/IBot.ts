import { IOrder } from './IOrder';
export interface BotInterface {
    executeOrder(order: IOrder): void;
}
