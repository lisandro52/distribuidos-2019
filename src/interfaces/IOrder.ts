export interface IOrder {
    siteName: string;
    categoryName: string;
    categoryUrls: string[];
}
