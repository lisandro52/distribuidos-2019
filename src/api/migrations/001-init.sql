-- Up

CREATE TABLE `scrap_data` (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    site TEXT NOT NULL,
    category TEXT NOT NULL,
    data TEXT NOT NULL
);