import DB from "better-sqlite3-helper";
import express from "express";
import config from "../config.json";

DB({
    path: './data/sqlite3.db',
    memory: false,
    readonly: false,
    fileMustExist: true,
    migrate: false
});

const app = express();

app.use(express.json());

function getAllScrapData() {
    return DB().query("SELECT * FROM scrap_data");
}

app.get(config.api_endpoint, (req, res, next) => {
    console.log("Getting data...");
    res.json(getAllScrapData());
});

app.post(config.api_endpoint, (req, res, next) => {

    console.log("Inserting data...");
    try {
        DB().insertWithBlackList("scrap_data", req.body, ["id"]);
        console.log("Data inserted succefully");
        res.sendStatus(200);
    } catch (e) {
        console.error(e);
        res.status(503).json({error: e.message});
    }

});

app.listen(config.api_port, () => {
    console.log(`API listening on port ${config.api_port}`);
});
