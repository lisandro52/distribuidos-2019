# Distribuidos 2019

## Pasos para la instalación:

- Instalar [Node.js](https://nodejs.org/en/) 12 o superior (incluye npm).
- Ejecutar ```npm install``` en el directorio de este README.
- Crear una base de datos SQLite3 en ```./data/sqlite3.db``` y ejecutar el query que está en ```./src/api/migrations/001-init.sql``` para crear la tabla necesaria.
- Levantar la API (``` npm run api ```), levantar Skynet (``` npm run start ```) y 1 o más bots, pasándole por parámetro el puerto a utilizar (``` npm run bot 8000 ```, por ejemplo).
